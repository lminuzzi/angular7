import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = environment.host + 'usuario'

  constructor(private http:HttpClient) { }

  public save(user:User):Observable<User> {
    return this.http.post<User>(this.url, user)
  }

  public delete(id:string):Observable<any> {
    return this.http.delete(this.url + "/" + id)
  }

  public getUser(): User {
    let user = new User()
    user.nome = "Luciano"
    user.email = "lucianopminuzzi@gmail.com"
    return user
  }

  public getUsers():Observable<User[]> {
    return this.http.get<User[]>(this.url)
  }
}