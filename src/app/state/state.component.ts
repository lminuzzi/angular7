import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {
  states:any = []
  constructor(private stateService:StateService) { }

  ngOnInit() {
    this.stateService.getStates().subscribe(
      response => {
        this.states = response
      }
    )
  }

}
