import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [ConfirmationService]
})

export class UserComponent implements OnInit {
  public singleUser:User
  public users:User[]
  constructor(private userService:UserService, 
    private confirmationService:ConfirmationService) { }

  ngOnInit() {
    this.singleUser = this.userService.getUser()
    this.getUsersList()
  }

  public getUsersList() {
    this.userService.getUsers().subscribe(
      response => {
        this.users = response
      },
      error => {
        console.log("************* getUsers ERROR.")
      }
    )
  }

  public delete(id:string) {
    this.confirmationService.confirm({
        message: 'Are you sure that you want to perform this action?',
        accept: () => {
          this.userService.delete(id).subscribe(
            response => {
              this.getUsersList()
            },
            error => {
              console.log("************* delete ERROR.")
            }
          )
        }
    });
  }
}
