import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/user';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['../user.component.css']
})

export class UserRegisterComponent implements OnInit {
  
  public user:User = new User() 
  constructor(private userService:UserService, private router: Router) { }

  ngOnInit() {
  }

  public save() {
    this.userService.save(this.user).subscribe(
      response => {
        console.log("User Save Successfully.")
        this.router.navigate(['/user']);
      }, 
      error => {
        console.log("************* User Save ERROR.")
      }
    )
  }
}