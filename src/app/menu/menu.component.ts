import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];
  constructor() { }

  ngOnInit() {
    this.items = [
        {
            
            label: 'Home', 
            icon: 'pi pi-fw pi-home',
            routerLink: 'home'
        },
        {
            label: 'User', 
            icon: 'pi pi-fw pi-user',
            routerLink: 'user',
            items: [
              {
                label: 'Add', 
                icon: 'pi pi-fw pi-user-plus',
                routerLink: 'user-register'
              },
              {
                label: 'Delete', 
                icon: 'pi pi-fw pi-user-minus'
              },
            ]
        },
        {
            label: 'State', 
            icon: 'pi pi-fw pi-globe',
            routerLink: 'state'
        },
        {
            label: 'Help', 
            icon: 'pi pi-fw pi-question',
            routerLink: 'help'
        }
    ];
  }
}
